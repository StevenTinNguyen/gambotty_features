package com.fsoft.gam.dci.gambottyapi;

import com.fsoft.gam.dci.gambottyapi.domain.User;
import com.fsoft.gam.dci.gambottyapi.repository.UserRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableScheduling
public class GambottyapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GambottyapiApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedOrigins("http://localhost:3000", "https://localhost:3000")
						.allowedHeaders("*")
						.allowedMethods("*");
			}
		};
	}

	@Bean
	public CommandLineRunner insertData(UserRepository repository) {
		return (args) -> {
			repository.save(new User("100074648138376", "GAM.VN.GOS TEST (15)", "GAM.VN.GOS TEST (15)", "GAM", null));
			repository.save(new User("100074299794766", "GAM.VN.GOS TEST (14)", "GAM.VN.GOS TEST (14)", "GAM", null));
			repository.save(new User("100074399178365", "GAM.VN.GOS TEST (13)", "GAM.VN.GOS TEST (13)", "GAM", null));
			repository.save(new User("100074690460499", "GAM.VN.GOS TEST (12)", "GAM.VN.GOS TEST (12)", "GAM", null));
			repository.save(new User("100074498953253", "GAM.VN.GOS TEST (11)", "GAM.VN.GOS TEST (11)", "GAM", null));
			repository.save(new User("100074769993157", "GAM.VN.GOS TEST (10)", "GAM.VN.GOS TEST (10)", "GAM", null));
			repository.save(new User("100074313143819", "GAM.VN.GOS TEST (9)", "GAM.VN.GOS TEST (9)", "GAM", null));
			repository.save(new User("100074785380448", "GAM.VN.GOS TEST (8)", "GAM.VN.GOS TEST (8)", "GAM", null));
			repository.save(new User("100074350882511", "GAM.VN.GOS TEST (7)", "GAM.VN.GOS TEST (7)", "GAM", null));
			repository.save(new User("100074593119560", "GAM.VN.GOS TEST (6)", "GAM.VN.GOS TEST (6)", "GAM", null));
			repository.save(new User("100074553461601", "GAM.VN.GOS TEST (5)", "GAM.VN.GOS TEST (5)", "GAM", null));
			repository.save(new User("100074650530450", "GAM.VN.GOS TEST (4)", "GAM.VN.GOS TEST (4)", "GAM", null));
			repository.save(new User("100074278435579", "GAM.VN.GOS TEST (3)", "GAM.VN.GOS TEST (3)", "GAM", null));
			repository.save(new User("100074390388530", "GAM.VN.GOS TEST (2)", "GAM.VN.GOS TEST (2)", "GAM", null));
			repository.save(new User("100074630378774", "GAM.VN.GOS TEST (1)", "GAM.VN.GOS TEST (1)", "GAM", null));
			// Production
			repository.save(new User("100012125890888", "VinhLQ", "La Quang Vinh (GAM.VN.GOS)", "GAM.VN.GOS", null));
			repository.save(new User("100012167196363", "QuangLT1", "Le Trong Quang (GAM.VN.BU0)", "GAM.VN.BU0", null));
			repository.save(new User("100012302803719", "AnhNM", "Nguyen Mai Anh (GAM.VN.GOS)", "GAM.VN.GOS", null));
			repository.save(
					new User("100015670692274", "KhanhNV16", "Nguyen Viet Khanh (GAM.VN.BU0)", "GAM.VN.BU0", null));
			repository.save(new User("100027149345416", "TanNM11", "Nguyen Minh Tan (GAM.VN.BU0)", "GAM.VN.BU0", null));
			repository.save(
					new User("100045036948035", "QuangNVN2", "Nguyen Vu Nhat Quang (FA.G0.DN.C)", "FA.G0.DN.C", null));
			repository.save(
					new User("100068335763275", "LongNT47", "Nguyen Thanh Long (GAM.VN.BU0)", "GAM.VN.BU0", null));
			repository.save(new User("100071675193519", "DUCTM", "Trần Minh Đức", "GAM.VN.BU0", null));
			repository.save(
					new User("100072914423247", "NgocNPT", "Nguyen Phan Tan Ngoc (FA.G0.DN.C)", "FA.G0.DN.C", null));
			repository.save(new User("100072951624029", "AnhCT8", "Cao Tuan Anh (GAM.DCI)", "GAM.DCI", null));
			repository.save(
					new User("100072980179684", "TinNVT", "Nguyen Van Trung Tin (GAM.VN.BU0)", "GAM.VN.BU0", null));
			repository.save(new User("100073215124956", "NhanTD3", "Tran Duy Nhan (FA.G0.DN.C)", "FA.G0.DN.C", null));
		};
	}

}
