package com.fsoft.gam.dci.gambottyapi.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

import com.fsoft.gam.dci.gambottyapi.domain.MessageUser;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelService {

  public ExcelService() {
  }

  public ByteArrayInputStream messageUsersToExcel(Set<MessageUser> messageUsers) {
    String[] headers = { "No.", "Account", "Status", "Fail Message" };

    try (Workbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream out = new ByteArrayOutputStream();) {
      Sheet sheet = workbook.createSheet("message_users");

      // Header
      Row headerRow = sheet.createRow(0);
      // Header style
      CellStyle headerStyle = workbook.createCellStyle();
      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerStyle.setFont(headerFont);

      for (int col = 0; col < headers.length; col++) {
        Cell cell = headerRow.createCell(col);
        cell.setCellValue(headers[col]);
        cell.setCellStyle(headerStyle);
      }

      // Danger style
      CellStyle style = workbook.createCellStyle();
      Font font = workbook.createFont();
      font.setColor(HSSFColor.HSSFColorPredefined.DARK_RED.getIndex());
      style.setFont(font);

      int rowIdx = 1;
      for (MessageUser messageUser : messageUsers) {
        Row row = sheet.createRow(rowIdx);

        row.createCell(0).setCellValue(rowIdx++);
        row.createCell(1).setCellValue(messageUser.getAccount());
        row.createCell(2).setCellValue(messageUser.getStatus());
        row.createCell(3).setCellValue(messageUser.getFailMessage());
        row.getCell(3).setCellStyle(style);
      }

      workbook.write(out);
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new RuntimeException("Failed to import data to Excel file: " + e.getMessage());
    }
  }
}
