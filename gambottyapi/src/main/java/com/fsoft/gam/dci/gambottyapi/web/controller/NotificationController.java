package com.fsoft.gam.dci.gambottyapi.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Payload;

import com.fsoft.gam.dci.gambottyapi.domain.Message;
import com.fsoft.gam.dci.gambottyapi.domain.MessageUser;
import com.fsoft.gam.dci.gambottyapi.domain.User;
import com.fsoft.gam.dci.gambottyapi.repository.MessageRepository;
import com.fsoft.gam.dci.gambottyapi.repository.MessageUserRepository;
import com.fsoft.gam.dci.gambottyapi.repository.UserRepository;
import com.fsoft.gam.dci.gambottyapi.service.GraphApiService;
import com.fsoft.gam.dci.gambottyapi.service.NotificationService;
import com.fsoft.gam.dci.gambottyapi.service.SurveyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private SurveyService surveyService;

    @Autowired
    private MessageRepository messageRepository;

    // @PostMapping("/survey")
    // public ResponseEntity<Map<String, Object>> sendSurveyApi(@RequestBody Message
    // message) {
    // Map<String, Object> result = new HashMap<>();
    // if (message.getId() != null) {
    // result.put("error", "Invalid ID");
    // return ResponseEntity.badRequest().body(result);
    // }
    // if (message.getListMessageUser() == null) {
    // result.put("error", "Message must have recipients");
    // return ResponseEntity.badRequest().body(result);
    // }
    // for (MessageUser messageUser : message.getListMessageUser()) {
    // messageUser.setMessage(message);
    // }
    // Message savedMessage = messageRepository.save(message);
    // //
    // long start = System.currentTimeMillis();
    // double successRate = surveyService.send(savedMessage);
    // long diff = System.currentTimeMillis() - start;
    // result.put("responseTime", diff);
    // result.put("message", "Sent to " + (successRate * 100) + "% users");
    // if (successRate >= 0.6) {
    // return ResponseEntity.ok().body(result);
    // } else {
    // return ResponseEntity.badRequest().body(result);
    // }
    // }

    @PostMapping("/notifications")
    public ResponseEntity<Map<String, Object>> sendNotifications(@RequestBody Message message) {
        Map<String, Object> result = new HashMap<>();
        //
        if (message.getId() != null) {
            result.put("error", "Invalid ID");
            return ResponseEntity.badRequest().body(result);
        }
        if (message.getListMessageUser() == null) {
            result.put("error", "Message must have recipients");
            return ResponseEntity.badRequest().body(result);
        }
        for (MessageUser messageUser : message.getListMessageUser()) {
            messageUser.setMessage(message);
        }
        Message savedMessage = messageRepository.save(message);
        //
        long start = System.currentTimeMillis();
        double successRate;
        if (savedMessage.getPayload() != null) {
            successRate = surveyService.send(savedMessage);
        } else {
            successRate = notificationService.send(savedMessage);
        }
        long diff = System.currentTimeMillis() - start;
        result.put("responseTime", diff);
        result.put("message", "Sent to " + (successRate * 100) + "% users");
        if (successRate >= 0.6) {
            return ResponseEntity.ok().body(result);
        } else {
            return ResponseEntity.badRequest().body(result);
        }
    }
}
